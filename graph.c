#include "graph.h"

graph_id_t& graph_id_add (
    graph_adapter_user_t    &self, 
    graph_id_data_t         &data,
    graph_id_t              &id
) {
    return self.interface.id.add(self.instance, data, id);
}
    
graph_id_t* graph_id_find (
    graph_adapter_user_t    &self, 
    graph_id_data_t         &data,
    const graph_id_t        &id
); {
    return self.interface.id.find(self.instance, data, id);
}  
    
bool graph_id_remove (
    graph_adapter_user_t    &self, 
    graph_id_data_t         &data,
    const graph_id_t        &id
) {
    return self.interface.id.remove(self.instance, data, id);
}


graph_attribute_t& graph_attribute_add (
    graph_adapter_user_t    &self, 
    graph_attribute_data_t  &data,
    graph_id_t              &id
) {
    return self.interface.attribute.add(self.instance, data, id);
}
    
graph_attribute_t* graph_attribute_find (
    graph_adapter_user_t    &self, 
    graph_attribute_data_t  &data,
    const graph_id_t        &id
) {
    return self.interface.attribute.find(self.instance, data, id);
}   
    
bool graph_attribute_remove (
    graph_adapter_user_t    &self, 
    graph_attribute_data_t  &data,
    const graph_id_t        &id
) {
    return self.interface.attribute.remove(self.instance, data, id);
}


graph_node_t& graph_node_add (
    graph_adapter_user_t    &self, 
    graph_node_data_t       &data,
    graph_id_t              &id
) {
    return self.interface.node.add(self.instance, data, id);
}
    
graph_node_t* graph_node_find (
    graph_adapter_user_t    &self, 
    graph_node_data_t       &data,
    const graph_id_t        &id
) {
    return self.interface.node.find(self.instance, data, id);
}
    
bool graph_node_remove (
    graph_adapter_user_t    &self, 
    graph_node_data_t       &data,
    const graph_id_t        &id
) {
    return self.interface.node.remove(self.instance, data, id);
}


graph_link_t& graph_link_add (
    graph_adapter_user_t    &self, 
    graph_link_data_t       &data,
    graph_id_t              &id
) {
    return self.interface.link.add(self.instance, data, id);
}
    
graph_link_t* graph_link_find (
    graph_adapter_user_t    &self, 
    graph_link_data_t       &data,
    const graph_id_t        &id
) {
    return self.interface.link.find(self.instance, data, id);
}
    
bool graph_link_remove (
    graph_adapter_user_t    &self, 
    graph_link_data_t       &data,
    const graph_id_t        &id
) {
    return self.interface.link.remove(self.instance, data, id);
}
    
    
graph_instance_t& graph_instance_add (
    graph_adapter_user_t    &self, 
    graph_instance_data_t   &data,
    graph_id_t              &id
) {
    return self.interface.instance.add(self.instance, data, id);
}
    
graph_instance_t* graph_instance_find (
    graph_adapter_user_t    &self, 
    graph_instance_data_t   &data,
    const graph_id_t        &id
) {
    return self.interface.instance.find(self.instance, data, id);
}
    
bool graph_instance_remove (
    graph_adapter_user_t    &self, 
    graph_instance_data_t   &data,
    const graph_id_t        &id
) {
    return self.interface.instance.remove(self.instance, data, id);
}



bool graph_visit (
    graph_visitor_user_t    &self, 
    graph_data_range_t      &data
) {
    bool b = true;
    
    graph_data_range_t visit_range;
    
    while (b && self.interface.data.prepare(self.instance, data, visit_range))
    {
        b = self.interface.data.visit(self.instance, visit_range);
    }
    
    return b;
}

bool graph_visit (
    graph_visitor_user_t    &self, 
    graph_root_data_t       &data
) {
    bool b = true;
    
    graph_root_range_t visit_range;
    
    while (b && self.interface.root.prepare(self.instance, data, visit_range))
    {
        b = self.interface.root.visit(self.instance, visit_range);
    }
    
    return b;
}

bool graph_visit (
    graph_visitor_user_t    &self, 
    graph_instance_data_t       &data
) {
    bool b = true;
    
    graph_instance_range_t visit_range;
    
    while (b && self.interface.instance.prepare(self.instance, data, visit_range))
    {
        b = self.interface.instance.visit(self.instance, visit_range);
    }
    
    return b;
}

bool graph_visit (
    graph_visitor_user_t    &self, 
    graph_node_data_t       &data
) {
    bool b = true;
    
    graph_node_range_t visit_range;
    
    while (b && self.interface.node.prepare(self.instance, data, visit_range))
    {
        b = self.interface.node.visit(self.instance, visit_range);
    }
    
    return b;
}

bool graph_visit (
    graph_visitor_user_t    &self, 
    graph_link_data_t       &data
) {
    bool b = true;
    
    graph_link_range_t visit_range;
    
    while (b && self.interface.link.prepare(self.instance, data, visit_range))
    {
        b = self.interface.link.visit(self.instance, visit_range);
    }
    
    return b;
}

bool graph_visit (
    graph_visitor_user_t    &self, 
    graph_attribute_data_t  &data
) {
    bool b = true;
    
    graph_attribute_range_t visit_range;
    
    while (b && self.interface.attribute.prepare(self.instance, data, visit_range))
    {
        b = self.interface.attribute.visit(self.instance, visit_range);
    }
    
    return b;
}

bool graph_visit (
    graph_visitor_user_t    &self, 
    graph_id_data_t         &data
) {
    bool b = true;
    
    graph_id_range_t visit_range;
    
    while (b && self.interface.id.prepare(self.instance, data, visit_range))
    {
        b = self.interface.id.visit(self.instance, visit_range);
    }
    
    return b;
}