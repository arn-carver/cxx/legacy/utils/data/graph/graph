#pragma once

typedef unsigned int                                            graph_flags_t;
typedef void*                                                   graph_user_data_t;
    
typedef unsigned int                                            graph_id_t;
typedef struct          graph_id_pack                           graph_id_pack_t;
typedef union           graph_id_union                          graph_id_union_t;
typedef struct          graph_id_set                            graph_id_set_t;
typedef struct          graph_id_range                          graph_id_range_t;
typedef struct          graph_id_data                           graph_id_data_t;
typedef struct          graph_id_data_adapter                   graph_id_data_adapter_t;
typedef struct          graph_id_data_visitor                   graph_id_data_visitor_t;


typedef unsigned long                                           graph_attribute_t;
typedef struct          graph_attribute_pack                    graph_attribute_pack_t;
typedef union           graph_attribute_union                   graph_attribute_union_t;
typedef struct          graph_attribute_range                   graph_attribute_range_t;
typedef struct          graph_attribute_data                    graph_attribute_data_t;
typedef struct          graph_attribute_data_adapter            graph_attribute_data_adapter_t;
typedef struct          graph_attribute_data_visitor            graph_attribute_data_visitor_t;

typedef struct          graph_node                              graph_node_t;
typedef struct          graph_node_range                        graph_node_range_t;
typedef struct          graph_node_data                         graph_node_data_t;
typedef struct          graph_node_data_adapter                 graph_node_data_adapter_t;
typedef struct          graph_node_data_visitor                 graph_node_data_visitor_t;

typedef unsigned int                                            graph_port_t;
typedef struct          graph_port_pack                         graph_port_pack_t;
typedef union           graph_port_union                        graph_port_union_t;

typedef struct          graph_link                              graph_link_t;
typedef struct          graph_link_range                        graph_link_range_t;
typedef struct          graph_link_data                         graph_link_data_t;
typedef struct          graph_link_data_adapter                 graph_link_data_adapter_t;
typedef struct          graph_link_data_visitor                 graph_link_data_visitor_t;
    
typedef struct          graph_instance                          graph_instance_t;
typedef struct          graph_instance_range                    graph_instance_range_t;
typedef struct          graph_instance_data                     graph_instance_data_t; 
typedef struct          graph_instance_data_adapter             graph_instance_data_adapter_t;
typedef struct          graph_instance_data_visitor             graph_instance_data_visitor_t;
typedef struct          graph_instance_list                     graph_instance_list_t;

typedef struct          graph_root                              graph_root_t;
typedef struct          graph_root_range                        graph_root_range_t;
typedef struct          graph_root_data                         graph_root_data_t;
typedef struct          graph_root_data_visitor                 graph_root_data_visitor_t;
    
typedef struct          graph_data                              graph_data_t;
typedef struct          graph_data_range                        graph_data_range_t;
typedef struct          graph_data_visitor                      graph_data_visitor_t;    
    
typedef struct          graph_adapter_interface                 graph_adapter_interface_t;
typedef struct          graph_adapter_user                      graph_adapter_user_t;
    
typedef struct          graph_visitor_interface                 graph_visitor_interface_t;
typedef struct          graph_visitor_user                      graph_visitor_user_t;

#ifndef graph_index_bits_k
#define graph_index_bits_k              24  //0-16,777,216
#endif

#ifndef graph_id_reserve_bits_k
#define graph_id_reserve_bits_k         8   //0-255
#endif

#ifndef graph_port_slot_bits_k
#define graph_port_slot_bits_k          5   //0-31
#endif

#ifndef graph_port_compass_bits_k
#define graph_port_compass_bits_k       3   //0-7
#endif

#ifndef graph_attribute_accessor_bits_k
#define graph_attribute_accessor_bits_k 16  //0-65,526
#endif



struct graph_id_pack
{
    graph_id_t  value   :graph_index_bits_k;    
    graph_id_t  reserve :graph_id_reserve_bits_k;     
};

union graph_id_union
{
    graph_id_t      data;
    graph_id_pack_t pack;
};

struct graph_id_set
{
    graph_id_t first;
    graph_id_t last;
};

struct graph_id_range
{
    graph_id_t* begin;
    graph_id_t* end;
};

struct graph_id_data
{
    graph_id_t          id;
    graph_id_range_t    range;
};

struct graph_id_data_adapter
{
    graph_id_t& (*add)      (graph_user_data_t&, graph_id_data_t&,       graph_id_t&);
    graph_id_t* (*find)     (graph_user_data_t&, graph_id_data_t&, const graph_id_t&);   
    bool        (*remove)   (graph_user_data_t&, graph_id_data_t&, const graph_id_t&);
};

struct graph_id_data_visitor
{
    bool(*prepare)  (graph_user_data_t&, graph_id_data_t&, graph_id_range_t&);
    bool(*visit)    (graph_user_data_t&, graph_id_range_t&);
};


struct graph_attribute_pack
{
    graph_id_t  key     :graph_index_bits_k;
    graph_id_t  value   :graph_index_bits_k;
    graph_id_t  accessor:graph_attribute_accessor_bits_k;
};

union graph_attribute_union
{
    graph_attribute_t       data;
    graph_attribute_pack    pack;
};

struct graph_attribute_range
{
    graph_attribute_t* begin;
    graph_attribute_t* end;
};

struct graph_attribute_data
{
    graph_id_t              id;
    graph_attribute_range_t range;
};

struct graph_attribute_data_adapter
{
    graph_attribute_t&  (*add)      (graph_user_data_t&, graph_attribute_data_t&,       graph_id_t&);
    graph_attribute_t*  (*find)     (graph_user_data_t&, graph_attribute_data_t&, const graph_id_t&);   
    bool                (*remove)   (graph_user_data_t&, graph_attribute_data_t&, const graph_id_t&);
};

struct graph_attribute_data_visitor
{
    bool(*prepare)  (graph_user_data_t&, graph_attribute_data_t&, graph_attribute_range_t&);
    bool(*visit)    (graph_user_data_t&, graph_attribute_range_t&);
};


struct graph_node
{
    graph_id_t      id;
    graph_id_set_t  attributes;
};

struct graph_node_range
{
    graph_node_t* begin;
    graph_node_t* end;
};

struct graph_node_data
{
    graph_id_t          id;
    graph_node_range_t  range;
};

struct graph_node_data_adapter
{
    graph_node_t&   (*add)      (graph_user_data_t&, graph_node_data_t&,        graph_id_t&);
    graph_node_t*   (*find)     (graph_user_data_t&, graph_node_data_t&, const  graph_id_t&);
    bool            (*remove)   (graph_user_data_t&, graph_node_data_t&, const  graph_id_t&);
};

struct graph_node_data_visitor
{
    bool(*prepare)  (graph_user_data_t&, graph_node_data_t&, graph_node_range_t&);
    bool(*visit)    (graph_user_data_t&, graph_node_range_t&);
};


struct graph_port_pack
{
    graph_id_t owner    :graph_index_bits_k;    
    graph_id_t slot     :graph_port_slot_bits_k;     
    graph_id_t compass  :graph_port_compass_bits_k;     
};

union graph_port_union
{
    graph_port_t        data;
    graph_port_pack_t   pack;
};


struct graph_link
{
    graph_port_t    from;
    graph_port_t    to;
    graph_id_set_t  attributes;
};

struct graph_link_range
{
    graph_link_t* begin;
    graph_link_t* end;
};

struct graph_link_data
{
    graph_id_t          id;
    graph_link_range_t  range;
};

struct graph_link_data_adapter
{
    graph_link_t&   (*add)      (graph_user_data_t&, graph_link_data_t&,        graph_id_t&);
    graph_link_t*   (*find)     (graph_user_data_t&, graph_link_data_t&, const  graph_id_t&);
    bool            (*remove)   (graph_user_data_t&, graph_link_data_t&, const  graph_id_t&);
};

struct graph_link_data_visitor
{
    bool(*prepare)  (graph_user_data_t&, graph_link_data_t&, graph_link_range_t&);
    bool(*visit)    (graph_user_data_t&, graph_link_range_t&);
};


struct graph_instance
{
    graph_id_t          id;
    graph_flags_t       flags;
    graph_id_set_t      attributes;
    graph_id_data_t     children;
    
    graph_link_list_t   links;
};

struct graph_instance_range
{
    graph_instance_t* begin;
    graph_instance_t* end;
};

struct graph_instance_data
{
    graph_id_t              id;
    graph_instance_range_t  range;
};

struct graph_instance_data_adapter
{
    graph_instance_t&   (*add)      (graph_user_data_t&, graph_instance_data_t&,        graph_id_t&);
    graph_instance_t*   (*find)     (graph_user_data_t&, graph_instance_data_t&, const  graph_id_t&);
    bool                (*remove)   (graph_user_data_t&, graph_instance_data_t&, const  graph_id_t&);
};

struct graph_instance_data_visitor
{
    bool(*prepare)  (graph_user_data_t&, graph_instance_data_t&, graph_instance_range_t&);
    bool(*visit)    (graph_user_data_t&, graph_instance_range_t&);
};


struct graph_root
{
    graph_node_list_t           nodes;
    graph_instance_list_t       instances;
    graph_attribute_data_t      attributes;
};

struct graph_root_range
{
    graph_root_t* begin;
    graph_root_t* end;
};

struct graph_root_data
{
    graph_id_t          id;
    graph_root_range_t  range;
};

struct graph_root_data_visitor
{
    bool(*prepare)  (graph_user_data_t&, graph_root_data_t&, graph_root_range_t&);
    bool(*visit)    (graph_user_data_t&, graph_root_range_t&);
};


struct graph_data
{
    graph_root_range_t     range;
    graph_adapter_user_t   adapter;
};

struct graph_data_range
{
    graph_data_t* begin;
    graph_data_t* end;
};

struct graph_data_visitor
{
    bool(*prepare)  (graph_user_data_t&, graph_data_range_t&, graph_data_range_t&);
    bool(*visit)    (graph_user_data_t&, graph_data_range_t&);
};


struct graph_adapter_interface
{  
    graph_instance_data_adapter_t   instance; 
    
    graph_node_data_adapter_t       node;
    graph_link_data_adapter_t       link;
    
    graph_attribute_data_adapter_t  attribute;  
    
    graph_id_data_adapter_t         id;
};

struct graph_adapter_user
{
    graph_user_data_t           instance;
    graph_adapter_interface_t   interface;
};


struct graph_visitor_interface
{
    graph_data_visitor_t            data;
    graph_root_data_visitor_t       root;
    
    graph_instance_data_visitor_t   instance;
    
    graph_node_data_visitor_t       node;
    graph_link_data_visitor_t       link;
    
    graph_attribute_data_visitor_t  attribute;
    
    graph_id_data_visitor_t         id;
};

struct graph_visitor_user
{
    graph_user_data_t           instance;
    graph_visitor_interface_t   interface;  
};


graph_id_t&                     graph_id_add                    (graph_adapter_user_t& self, graph_id_data_t& data,                    graph_id_t& id);
graph_id_t*                     graph_id_find                   (graph_adapter_user_t& self, graph_id_data_t& data,              const graph_id_t& id);   
bool                            graph_id_remove                 (graph_adapter_user_t& self, graph_id_data_t& data,              const graph_id_t& id);

graph_attribute_t&              graph_attribute_add             (graph_adapter_user_t& self, graph_attribute_data_t& data,             graph_id_t& id);
graph_attribute_t*              graph_attribute_find            (graph_adapter_user_t& self, graph_attribute_data_t& data,       const graph_id_t& id);   
bool                            graph_attribute_remove          (graph_adapter_user_t& self, graph_attribute_data_t& data,       const graph_id_t& id);

graph_node_t&                   graph_node_add                  (graph_adapter_user_t& self, graph_node_data_t& data,                  graph_id_t& id);
graph_node_t*                   graph_node_find                 (graph_adapter_user_t& self, graph_node_data_t& data,            const graph_id_t& id);
bool                            graph_node_remove               (graph_adapter_user_t& self, graph_node_data_t& data,            const graph_id_t& id);

graph_link_t&                   graph_link_add                  (graph_adapter_user_t& self, graph_link_data_t& data,                  graph_id_t& id);
graph_link_t*                   graph_link_find                 (graph_adapter_user_t& self, graph_link_data_t& data,            const graph_id_t& id);
bool                            graph_link_remove               (graph_adapter_user_t& self, graph_link_data_t& data,            const graph_id_t& id);
    
graph_instance_t&               graph_instance_add              (graph_adapter_user_t& self, graph_instance_data_t& data,              graph_id_t& id);
graph_instance_t*               graph_instance_find             (graph_adapter_user_t& self, graph_instance_data_t& data,        const graph_id_t& id);
bool                            graph_instance_remove           (graph_adapter_user_t& self, graph_instance_data_t& data,        const graph_id_t& id);
    

bool graph_visit (graph_visitor_user_t& self, graph_data_range_t            &data);
bool graph_visit (graph_visitor_user_t& self, graph_root_data_t             &data);
bool graph_visit (graph_visitor_user_t& self, graph_instance_data_t         &data);
bool graph_visit (graph_visitor_user_t& self, graph_node_data_t             &data);
bool graph_visit (graph_visitor_user_t& self, graph_link_data_t             &data);
bool graph_visit (graph_visitor_user_t& self, graph_attribute_data_t        &data);
bool graph_visit (graph_visitor_user_t& self, graph_id_data_t               &data);
